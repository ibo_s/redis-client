<?php
namespace PHPRedisClient\Redis\Protocol\Serializer;

use Clue\Redis\Protocol\Serializer\SerializerInterface as ClueSerializerInterface;

/**
 * Class RecursiveSerializer
 *
 * PHP Version 5
 *
 * @category  PHP
 */
class RecursiveSerializer implements SerializerInterface
{
    const CRLF = "\r\n";

    protected $wrappedSerializer;

    /**
     * RecursiveSerializer constructor.
     *
     * @param ClueSerializerInterface $wrappedSerializer The wrapped serializer.
     */
    public function __construct(ClueSerializerInterface $wrappedSerializer)
    {
        $this->wrappedSerializer = $wrappedSerializer;
    }

    /**
     * Get request message
     *
     *
     * @param       $command
     * @param array $args
     * @param array $options
     *
     * @return string
     */
    public function getRequestMessage($command, array $args = array(), array $options = array())
    {
        $argsCount = count($args);
        $optionsCount = 0;
        $optionsValueCount = 0;
        $optionsData = '';
        foreach ($options as $option => $value) {
            if ($value) {
                $optionsCount++;
                $optionsData .= '$' . strlen($option) . self::CRLF . $option . self::CRLF;
                if (
                    !is_bool($value) &&
                    !is_array($value) &&
                    (!is_object($value) || (is_object($value) && method_exists($value, '__toString')))
                ) {
                    $optionsValueCount++;
                    $optionsData .= '$' . strlen((string)$value) . self::CRLF . $value . self::CRLF;
                }
            }
        }
        $count = 1 + $argsCount + $optionsCount + $optionsValueCount;
        $data = '*' . $count . "\r\n$" . strlen($command) . self::CRLF . $command . self::CRLF;
        foreach ($args as $arg) {
            $data .= '$' . strlen($arg) . self::CRLF . $arg . self::CRLF;
        }
        $data .= $optionsData;

        return $data;
    }

    /**
     * Create request model
     *
     *
     * @param       $command
     * @param array $args
     *
     * @return \Clue\Redis\Protocol\Model\MultiBulkReply
     */
    public function createRequestModel($command, array $args = array())
    {

        return $this->wrappedSerializer->createRequestModel($command, $args);
    }

    /**
     * Get reply message
     *
     *
     * @param $data
     *
     * @return string
     */
    public function getReplyMessage($data)
    {

        return $this->wrappedSerializer->getReplyMessage($data);
    }

    /**
     * Create reply model
     *
     *
     * @param $data
     *
     * @return \Clue\Redis\Protocol\Model\ModelInterface
     */
    public function createReplyModel($data)
    {

        return $this->wrappedSerializer->createReplyModel($data);
    }

    /**
     * Get bulk message
     *
     *
     * @param $data
     *
     * @return mixed
     */
    public function getBulkMessage($data)
    {

        return $this->wrappedSerializer->getBulkMessage($data);
    }

    /**
     * Get error message
     *
     *
     * @param $data
     *
     * @return mixed
     */
    public function getErrorMessage($data)
    {

        return $this->wrappedSerializer->getErrorMessage($data);
    }

    /**
     * Get integer message
     *
     *
     * @param $data
     *
     * @return mixed
     */
    public function getIntegerMessage($data)
    {

        return $this->wrappedSerializer->getIntegerMessage($data);
    }

    /**
     * Get multi bulk message
     *
     *
     * @param $data
     *
     * @return mixed
     */
    public function getMultiBulkMessage($data)
    {

        return $this->wrappedSerializer->getMultiBulkMessage($data);
    }

    /**
     * Get status message
     *
     *
     * @param $data
     *
     * @return mixed
     */
    public function getStatusMessage($data)
    {

        return $this->wrappedSerializer->getStatusMessage($data);
    }
}