<?php
namespace PHPRedisClient\Client;

/**
 * Class Configuration
 *
 * PHP Version 5
 *
 * @category  PHP
 * @package   PHPRedisClient\Client
 */
class Configuration implements ConfigurationInterface
{
    /** @var string */
    protected $protocol = 'tcp://';

    /** @var string */
    protected $host = 'localhost';

    /** @var integer */
    protected $port = '6379';

    /** @var boolean */
    protected $enableWriteConnection = false;

    /**
     * @return string
     */
    public function getProtocol()
    {

        return $this->protocol;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return int
     */
    public function getPort()
    {

        return $this->port;
    }

    /**
     * @param integer $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getHost()
    {

        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * Get enable write connection
     *
     *
     * @return boolean
     */
    public function getEnableWriteConnection()
    {

        return $this->enableWriteConnection;
    }

    /**
     * @param boolean $enableWriteConnection
     */
    public function setEnableWriteConnection($enableWriteConnection)
    {
        $this->enableWriteConnection = $enableWriteConnection;
    }

    /**
     * Configuration constructor.
     *
     * @param array $config The configuration.
     */
    public function __construct(array $config = array())
    {
        if (isset($config['protocol'])) {
            $this->setProtocol($config['protocol']);
        }

        if (isset($config['host'])) {
            $this->setHost($config['host']);
        }

        if (isset($config['port'])) {
            $this->setPort(intval($config['port']));
        }

        if (isset($config['enableWriteConnection'])) {
            $this->setEnableWriteConnection(boolval($config['enableWriteConnection']));
        } elseif (isset($config['enable_write_connection'])) {
            $this->setEnableWriteConnection(boolval($config['enable_write_connection']));
        }
    }
}