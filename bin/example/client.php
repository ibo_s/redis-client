<?php
require __DIR__ . '/../../vendor/autoload.php';
ini_set('display_errors', 1);

$extra = null;
if (isset($argv[1])) {
    $extra = $argv[1];
}

runExample($extra);

/**
 * @return \PHPRedisClient\Client
 */
function getClient()
{
    $factory = new \Clue\Redis\Protocol\Factory();
    $recursiveSerializerFactory = new \PHPRedisClient\Redis\Protocol\Serializer\RecursiveSerializerFactory();
    $serializer = $recursiveSerializerFactory->create();
    $parser = $factory->createResponseParser();

    // change enable_write_connection value to have better performance for many "set" operations
    $clientConfiguration = new \PHPRedisClient\Client\Configuration(array('enable_write_connection' => true));
    $client = new \PHPRedisClient\Client($clientConfiguration, $serializer, $parser);

    return $client;
}

/**
 * The runExample function will be started at each request.
 * In order to run additional demonstrations, just add any parameter to your cli call.
 *
 * @param string $extra Do additional actions when an additional cli param is set
 *
 * @return void
 */
function runExample($extra = null)
{
    $client = getClient();

    echo PHP_EOL;
    helperDotsAnimation();
    echo 'Example: "INCR" "DECR" "INCRBY" "DECRBY" methods' . PHP_EOL;
    $client->mSet(array('x' => '10', 'y' => 10, 'z' => 'foo'));
    // 10
    echo $client->get('x') . PHP_EOL;
    // 10
    echo $client->get('y') . PHP_EOL;
    // 9
    echo $client->decrBy('x', 1) . PHP_EOL;
    // 8
    echo $client->decrBy('x', 1) . PHP_EOL;
    // 8
    echo $client->get('x') . PHP_EOL;
    // 10
    echo $client->incrBy('x', 2) . PHP_EOL;
    $client->set('y', '10');
    // 10
    echo $client->get('y') . PHP_EOL;
    // 2
    echo $client->decrBy('y', '8') . PHP_EOL;
    // 2
    echo $client->get('y') . PHP_EOL;
    // 5
    echo $client->incrBy('y', '3') . PHP_EOL;
    // 5
    echo $client->get('y') . PHP_EOL;
    // 10
    echo $client->incrBy('y', 5) . PHP_EOL;
    // 12
    echo $client->incrBy('y', 2) . PHP_EOL;
    // -3
    echo $client->decrBy('y', 15) . PHP_EOL;
    // -2
    echo $client->incrBy('y', 1) . PHP_EOL;
    // -1
    echo $client->incrBy('y', 1) . PHP_EOL;
    // 10
    echo $client->incrBy('y', 11) . PHP_EOL;
    // ERR value is not an integer or out of range
    echo $client->incrBy('z', 10) . PHP_EOL;
    // 11
    echo $client->incr('x') . PHP_EOL;
    // 10
    echo $client->decr('x') . PHP_EOL;
    // 11
    echo $client->incr('y') . PHP_EOL;
    // 10
    echo $client->decr('y') . PHP_EOL;
    // 9.9
    echo $client->incrByFloat('y', -0.1) . PHP_EOL;
    // 10
    echo $client->incrByFloat('y', 0.1) . PHP_EOL;

    echo PHP_EOL;
    helperDotsAnimation();
    echo 'Example: "set" and "get" method' . PHP_EOL;

    $client->set('foobar', 'Foo Bar!');
    echo 'getting foobar ...' . PHP_EOL;
    echo $client->get('foobar') . PHP_EOL;

    $client->set('helloworld', 'Hello World! ' . $extra . '!');
    echo 'getting helloworld ...' . PHP_EOL;
    echo $client->get('helloworld') . PHP_EOL;

    echo 'getting nonexisting ...' . PHP_EOL;
    echo $client->get('nonexisting') . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "set" in a loop' . PHP_EOL;

    // prepare loop tests
    $loopCount = 50000;
    $client->set('i', 0);

    // loop with set operation
    $starttime = microtime(true);
    for ($i = 0; $i < $loopCount; $i++) {
        $client->set('i', $i + 1);
    }
    $endtime = microtime(true);
    $processTime = $endtime - $starttime;
    echo 'processed redis loop in: ' . $processTime;
    echo ' (loop count: ' . $i . ')' . PHP_EOL;

    //// loop with setFast operation
    //$starttime = microtime(true);
    //for ($i = 0; $i < $loopCount; $i++) {
    //    $client->setFast('i', $i + 1);
    //}
    //$client->flushSetFastQueue();
    //$endtime = microtime(true);
    //$processTime = $endtime - $starttime;
    //echo 'processed redis loop with "setFast" in: ' . $processTime;
    //echo ' (loop count: ' . $i . ')' . PHP_EOL;

    // loop with assign and calculate operation
    $starttime = microtime(true);
    for ($i = 0; $i < $loopCount; $i++) {
        $y = $i + 1;
    }
    $endtime = microtime(true);
    $processTime = $endtime - $starttime;

    echo 'processed native loop in: ' . $processTime;
    echo ' (loop count: ' . $i . ')' . PHP_EOL;

    if (!$extra) {
        helperExit();
    }

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "TIME' . PHP_EOL;
    echo var_dump($client->time()) . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "LPUSH" with key "somelist" and value "listing1" and "LINDEX"' . PHP_EOL;

    $client->lPush('somelist', 'listing1');
    echo 'get index "0" from "somelist" with "LINDEX"' . PHP_EOL;
    echo $client->lIndex('somelist', 0) . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "LPOP" -> "LPUSH" -> "LPUSH" -> "LPOP" -> "LPOP" -> "LPOP" -> "LPOP" with key "anotherlist"' . PHP_EOL;

    echo $client->lPop('anotherlist') . PHP_EOL;
    $client->lPush('anotherlist', '>>1. Value<<');
    $client->lPush('anotherlist', '>>2. Value<<');
    echo $client->lPop('anotherlist') . PHP_EOL;
    echo $client->lPop('anotherlist') . PHP_EOL;
    echo $client->lPop('anotherlist') . PHP_EOL;
    echo $client->lPop('anotherlist') . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "LPUSH" -> "LPUSH" -> "LPUSH" -> "RPOP" -> "RPOP" -> "RPOP" with key "anotherlist"' . PHP_EOL;

    $client->lPush('anotherlist', '>>1. Value<<');
    $client->lPush('anotherlist', '>>2. Value<<');
    $client->lPush('anotherlist', '>>3. Value<<');
    echo $client->rPop('anotherlist') . PHP_EOL;
    echo $client->rPop('anotherlist') . PHP_EOL;
    echo $client->rPop('anotherlist') . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: "RPUSH" -> "RPUSH" -> "RPUSH" -> "RPUSH" -> "LPOP" -> "LPOP" -> "RPOP" -> "LPOP"' . PHP_EOL;

    $client->rPush('anotherlist', '>>1. Value<<');
    $client->rPush('anotherlist', '>>2. Value<<');
    $client->rPush('anotherlist', '>>3. Value<<');
    $client->rPush('anotherlist', '>>4. Value<<');
    echo $client->lPop('anotherlist') . PHP_EOL;
    echo $client->lPop('anotherlist') . PHP_EOL;
    echo $client->rPop('anotherlist') . PHP_EOL;
    echo $client->lPop('anotherlist') . PHP_EOL;

    echo PHP_EOL;
    helperWait();
    helperDotsAnimation();
    echo 'Example: Set a key with expiry, wait a second and read ttl "SET EX 10"' . PHP_EOL;

    $client->set('expiringkey', 'abcdefghijklmnopqrstuvwxyz! ()<>?=[]¶¢Ω', 10);
    sleep(1);
    echo 'TTL: ' . $client->ttl('expiringkey') . PHP_EOL;

    echo PHP_EOL;
    helperWait(1);
    helperExit();
}

function helperDotsAnimation($sleepTime = 1)
{
    echo 'Starting example ';
    sleep($sleepTime);
    echo '.';
    sleep($sleepTime);
    echo '.';
    echo '.';
    sleep($sleepTime);
    echo PHP_EOL;
}

function helperWait($sleepTime = 0)
{
    sleep($sleepTime);
}

function helperExit()
{
    echo PHP_EOL . 'finished!' . PHP_EOL;
    exit(1);
}