<?php
namespace PHPRedisClient\Client;

/**
 * Interface ConfigurationInterface
 *
 * PHP Version 5
 *
 * @category  PHP
 * @package   PHPRedisClient\Client
 */
interface ConfigurationInterface
{
    /**
     * Get protocol
     *
     *
     * @return string
     */
    public function getProtocol();

    /**
     * Get host
     *
     *
     * @return string
     */
    public function getHost();

    /**
     * Get port
     *
     *
     * @return integer
     */
    public function getPort();

    /**
     * Get enable write connection
     *
     *
     * @return boolean
     */
    public function getEnableWriteConnection();
}