<?php
namespace PHPRedisClient\Redis\Protocol\Serializer;

use Clue\Redis\Protocol\Factory;

/**
 * Class RecursiveSerializerFactory
 *
 * PHP Version 5
 *
 * @category  PHP
 * @package   PHPRedisClient\Redis\Protocol\Serializer
 * @author    Simplicity Trade GmbH <development@simplicity.ag>
 * @copyright 2014-2016 Simplicity Trade GmbH
 * @license   Proprietary http://www.simplicity.ag
 */
class RecursiveSerializerFactory
{
    /**
     * Create recursive serializer
     *
     *
     * @return RecursiveSerializer
     */
    public function create()
    {
        $clueFactory = new Factory();
        return new RecursiveSerializer($clueFactory->createSerializer());
    }
}