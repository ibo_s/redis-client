<?php
namespace PHPRedisClient;

use Clue\Redis\Protocol\Model\ModelInterface;
use Clue\Redis\Protocol\Parser\ParserInterface;
use PHPRedisClient\Client\ConfigurationInterface;
use PHPRedisClient\Redis\Protocol\Serializer\SerializerInterface;

/**
 * Class Client
 *
 * PHP Version 5
 *
 * @category  PHP
 * @package   PHPRedisClient
 */
class Client
{
    /** @var ConfigurationInterface */
    protected $configuration;

    /** @var string */
    protected $socketAddress;

    /** @var resource */
    protected $connection;

    /** @var resource */
    protected $writeConnection;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var ParserInterface */
    protected $parser;

    /** @var array */
    protected $setFastQueue = array();

    /**
     * Client constructor.
     *
     * @param ConfigurationInterface $configuration The configuration.
     * @param SerializerInterface    $serializer    The serializer.
     * @param ParserInterface        $parser        The parser.
     */
    public function __construct(
        ConfigurationInterface $configuration,
        SerializerInterface $serializer,
        ParserInterface $parser
    )
    {
        $this->configuration = $configuration;
        $this->serializer = $serializer;
        $this->parser = $parser;
        $this->enableWriteConnection = $this->configuration->getEnableWriteConnection();
    }

    /**
     * set
     *
     * @param string  $key   The key.
     * @param mixed   $value The value.
     * @param integer $ex    The expiry time in s (optional).
     * @param integer $px    The expiry time in ms (optional).
     * @param boolean $nx    Only set key if it does not exist.
     * @param boolean $xx    Only set key it it already exists.
     *
     * @return void
     */
    public function set($key, $value, $ex = null, $px = null, $nx = false, $xx = false)
    {
        $options = compact('ex', 'px', 'nx', 'xx');
        fwrite($this->getWriteConnection(), $this->serializer->getRequestMessage('SET', array($key, $value), $options));
        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * multi set
     *
     *
     * @param array $keyValuePairs The key value pairs to set.
     *
     * @return void
     */
    public function mSet(array $keyValuePairs)
    {
        $normalizedKeyValue = array();
        foreach ($keyValuePairs as $key => $value) {
            if (
                $value !== null &&
                (is_scalar($value) || (is_object($value) && method_exists($value, '__toString')))
            ) {
                $normalizedKeyValue[] = $key;
                $normalizedKeyValue[] = $value;
            }
        }
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('MSET', $normalizedKeyValue));
        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * Prepare options command
     *
     *
     * @param array $options The command options.
     *
     * @return string | null
     */
    protected function prepareOptionsCommand(array $options)
    {
        $preparedOptionsCommand = '';
        foreach ($options as $option => $value) {
            if ($value) {
                $preparedOptionsCommand .= strtoupper($option) . ' ';
                if (
                    !is_bool($value) &&
                    !is_array($value) &&
                    (!is_object($value) || (is_object($value) && method_exists($value, '__toString')))
                ) {
                    $preparedOptionsCommand .= $value . ' ';
                }
            }
        }

        $preparedOptionsCommand = trim($preparedOptionsCommand);

        if ($preparedOptionsCommand != '') {

            return $preparedOptionsCommand;
        }

        return null;
    }

    /**
     * Get write connection
     *
     * @return resource
     */
    public function getWriteConnection()
    {
        if ($this->configuration->getEnableWriteConnection()) {
            if (!$this->writeConnection || $this->writeConnection == null) {
                $this->writeConnection = fsockopen($this->getSocketAddress(), $this->getSocketPort());
            }

            return $this->writeConnection;
        }

        return $this->getConnection();
    }

    /**
     * Get socket address
     *
     * @return string
     */
    public function getSocketAddress()
    {
        if (!$this->socketAddress) {
            $this->socketAddress = $this->configuration->getProtocol() . $this->configuration->getHost();
        }

        return $this->socketAddress;
    }

    /**
     * Get socket port
     *
     * @return integer
     */
    public function getSocketPort()
    {

        return $this->configuration->getPort();
    }

    /**
     * Get connection
     *
     * @return resource
     */
    public function getConnection()
    {
        if (!$this->connection || $this->connection == null) {
            $this->connection = fsockopen($this->getSocketAddress(), $this->getSocketPort());
        }

        return $this->connection;
    }

    /**
     * Decrement by
     *
     *
     * @param string  $key       The key.
     * @param integer $decrement The decrement value.
     *
     * @return string
     */
    public function decrBy($key, $decrement)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('DECRBY', array($key, $decrement)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * Increment by
     *
     *
     * @param string  $key       The key.
     * @param integer $increment The increment value.
     *
     * @return string
     */
    public function incrBy($key, $increment)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('INCRBY', array($key, $increment)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * Decrement
     *
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function decr($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('DECR', array($key)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * Increment by
     *
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function incr($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('INCR', array($key)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * increment by float
     *
     *
     * @param string $key       The key.
     * @param float  $increment The float value to increment by.
     *
     * @return string
     */
    public function incrByFloat($key, $increment)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('INCRBYFLOAT', array($key, $increment)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * save redis state asynchronously in background
     *
     * @return void
     */
    public function bgSave()
    {
        fwrite($this->getWriteConnection(), $this->serializer->getRequestMessage('BGSAVE'));

        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * rename a key
     *
     *
     * @param string $key    The key.
     * @param string $newKey The new key.
     *
     * @return void
     */
    public function rename($key, $newKey)
    {
        fwrite($this->getWriteConnection(), $this->serializer->getRequestMessage('RENAME', array($key, $newKey)));

        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * time to live in milliseconds
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function pTtl($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('PTTL', array($key)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * time from redis
     * returns an array with two values
     * 0 => unix time in seconds
     * 1 => remainder microseconds
     *
     *
     * @return array
     */
    public function time()
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('TIME'));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * time to live in seconds
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function ttl($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('TTL', array($key)));
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * Get first value from models
     *
     *
     * @param ModelInterface[] $models Set of models.
     *
     * @return mixed|string
     */
    protected function firstValueFromModels($models)
    {
        /** @var ModelInterface $reply */
        $reply = array_pop($models);

        if ($reply) {

            return $reply->getValueNative();
        }

        return '';
    }

    /**
     * list left push
     *
     *
     * @param string $key   The key.
     * @param string $value The value.
     *
     * @return void
     */
    public function lPush($key, $value)
    {
        fwrite($this->getWriteConnection(), $this->serializer->getRequestMessage('LPUSH', array($key, $value)));

        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * list right push
     *
     *
     * @param string $key   The key.
     * @param string $value The value.
     *
     * @return void
     */
    public function rPush($key, $value)
    {
        fwrite($this->getWriteConnection(), $this->serializer->getRequestMessage('RPUSH', array($key, $value)));

        if (!$this->configuration->getEnableWriteConnection()) {
            fread($this->connection, 4096);
        }
    }

    /**
     * list index
     *
     *
     * @param string  $key   The key.
     * @param integer $index The index to retrieve value from.
     *
     * @return string
     */
    public function lIndex($key, $index)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('LINDEX', array($key, $index)));
        // TODO: make maximum key length configurable
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * list left pop
     *
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function lPop($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('LPOP', array($key)));
        // TODO: make maximum key length configurable
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * list right pop
     *
     *
     * @param string $key The key.
     *
     * @return string
     */
    public function rPop($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('RPOP', array($key)));
        // TODO: make maximum key length configurable
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * get
     *
     *
     * @param string $key The key.
     *
     * @return mixed
     */
    public function get($key)
    {
        fwrite($this->getConnection(), $this->serializer->getRequestMessage('GET', array($key)));
        // TODO: make maximum key length configurable
        $models = $this->parser->pushIncoming(fread($this->getConnection(), 4096));

        return $this->firstValueFromModels($models);
    }

    /**
     * Destruct client to close connections
     */
    public function __destruct()
    {
        if ($this->connection) {
            fclose($this->connection);
        }

        if ($this->writeConnection) {
            fclose($this->writeConnection);
        }
    }

    /**
     * set fast
     *
     *
     * @param string $key     The key.
     * @param mixed  $value   The value.
     * @param array  $options The options.
     *
     * @return void
     */
    public function setFast($key, $value, $options = array())
    {
        $this->setFastQueue[] = $this->serializer->getRequestMessage('SET', array($key, $value), $options);
    }

    /**
     * flush set fast queue to send all request from queue to redis
     *
     * @return void
     */
    public function flushSetFast()
    {
        $setFastCount = count($this->setFastQueue);
        fwrite($this->getConnection(), implode($this->setFastQueue));
        fread($this->connection, $setFastCount * 6);
    }

}