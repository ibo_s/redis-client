<?php
namespace PHPRedisClient\Redis\Protocol\Serializer;

interface SerializerInterface
{
    public function getRequestMessage($command, array $args = array(), array $options = array());

    public function createRequestModel($command, array $args = array());

    public function getReplyMessage($data);

    public function createReplyModel($data);

    public function getBulkMessage($data);

    public function getErrorMessage($data);

    public function getIntegerMessage($data);

    public function getMultiBulkMessage($data);

    public function getStatusMessage($data);
}
